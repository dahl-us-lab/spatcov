from distutils.core import setup
from distutils.extension import Extension
from Cython.Build import cythonize
import numpy as np

ext_modules=[
    Extension("spatcov",
              sources=["spatcov.pyx"],
              libraries=["../lib/spatcov"],
              language="c++")
]

setup(
    name = "spatcov",
    ext_modules = cythonize(ext_modules),
)

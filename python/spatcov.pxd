cdef extern from "../include/spatcov.h":
    double checkDataRMS[T](T *I, T *Q, size_t dataSize)
    void spatcov[T](int ccmode,
                    size_t ndims,
                    const size_t *dataDims,
                    T *I,
                    T *Q,
                    size_t *kernDims,
                    T *ccR,
                    T *ccI,
                    size_t nlags,
                    size_t *lagSizes,
                    size_t *ccIndexes)

# void spatcov(int ccmode,
# 			 size_t ndims,
# 			 const size_t *dataDims,
# 			 T *I,
# 			 T *Q,
# 			 size_t *kernDims,
# 			 T *ccR,
# 			 T *ccI,
# 			 size_t nlags,
# 			 size_t *lagSizes=NULL,
# 			 size_t *ccIndexes=NULL);
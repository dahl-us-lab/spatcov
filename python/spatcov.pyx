from spatcov cimport checkDataRMS
import numpy as np
cimport numpy as np

def computeDataRMS(I, Q):
    assert I.dtype == Q.dtype, "I and Q must be of the same datatype."
    cdef np.ndarray[float ,mode='c'] If
    cdef np.ndarray[float ,mode='c'] Qf
    cdef np.ndarray[double,mode='c'] Id
    cdef np.ndarray[double,mode='c'] Qd
    cdef size_t dataSize = I.size
    if I.dtype == np.dtype('float32'):
        If = np.ascontiguousarray(I)
        Qf = np.ascontiguousarray(Q)
        return checkDataRMS[float](<float *>If.data, <float *>Qf.data, dataSize)
    elif I.dtype == np.dtype('float64'):
        Id = np.ascontiguousarray(I)
        Qd = np.ascontiguousarray(Q)
        return checkDataRMS[double](<double *>Id.data, <double *>Qd.data, dataSize)

def computeSpatialCovariance(int ccmode,
                             I,
                             Q,
                             kernDims,
                             size_t nlags,
                             customLags=None,
                             computeComplexCov=False):
    # Check inputs
    assert I.dtype == Q.dtype, "I and Q must be of the same datatype."
    assert I.shape == Q.shape, "I and Q must have the same shape."
    assert isinstance(kernDims, (list, tuple)), "kernDims must be a list or tuple."

    # Fill out the rest of kernel dimensions if not provided
    if kernDims is None:
        kernDims = []
    if len(kernDims) < I.ndim:
        kernDims = list(kernDims) # Convert to list in case of tuple
        for d in range(len(kernDims), I.ndim):
            kernDims.append(1) # Assume no kernel in unfilled dimensions

    # Initialize C types
    cdef:
        # Scalars
        int mode = ccmode
        size_t ndims = I.ndim
        size_t nlag = nlags
        # Arrays in the case of single precision
        np.ndarray[float ,mode='c'] If
        np.ndarray[float ,mode='c'] Qf
        np.ndarray[float ,mode='c'] ccRf
        np.ndarray[float ,mode='c'] ccIf
        # Arrays in the case of double precision
        np.ndarray[double,mode='c'] Id
        np.ndarray[double,mode='c'] Qd
        np.ndarray[double,mode='c'] ccRd
        np.ndarray[double,mode='c'] ccId
        # Dimensions of data and kernel
        np.ndarray[size_t,mode='c'] ddims = np.ascontiguousarray(np.array(I.shape,dtype=np.uintp))
        np.ndarray[size_t,mode='c'] kdims = np.ascontiguousarray(np.array(kernDims,dtype=np.uintp))
        # Custom lag definitions
        np.ndarray[size_t,mode='c'] lagsz
        np.ndarray[size_t,mode='c'] ccidx


    if customLags == None:
        if I.dtype == np.dtype('float32'):
            # Initialize cdef arrays
            If = np.ascontiguousarray(I)
            Qf = np.ascontiguousarray(Q)
            ccRf = np.ascontiguousarray(np.zeros((I.shape[:-1] + (nlags,)),dtype=np.float32))
            if computeComplexCov == False:
                spatcov[float](mode,
                               ndims,
                               <size_t *>ddims.data,
                               <float  *>If.data,
                               <float  *>Qf.data,
                               <size_t *>kdims.data,
                               <float  *>ccRf.data,
                               NULL,
                               nlags)
                return ccRf
            else:
                ccIf = np.ascontiguousarray(np.zeros((I.shape[:-1] + (nlags,)),dtype=np.float32))
                spatcov[float](mode,
                               ndims,
                               <size_t *>ddims.data,
                               <float  *>If.data,
                               <float  *>Qf.data,
                               <size_t *>kdims.data,
                               <float  *>ccRf.data,
                               <float  *>ccIf.data,
                               nlags)
                return ccRf, ccIf
        elif I.dtype == np.dtype('float64'):
            # Initialize cdef arrays
            Id = np.ascontiguousarray(I)
            Qd = np.ascontiguousarray(Q)
            ccRd = np.ascontiguousarray(np.zeros((I.shape[:-1] + (nlags,)),dtype=np.float64))
            if computeComplexCov == False:
                spatcov[double](mode,
                                ndims,
                                <size_t *>ddims.data,
                                <double *>Id.data,
                                <double *>Qd.data,
                                <size_t *>kdims.data,
                                <double *>ccRd.data,
                                NULL,
                                nlags)
                return ccRd
            else:
                ccId = np.ascontiguousarray(np.zeros((I.shape[:-1] + (nlags,)),dtype=np.float64))
                spatcov[double](mode,
                                ndims,
                                <size_t *>ddims.data,
                                <double *>Id.data,
                                <double *>Qd.data,
                                <size_t *>kdims.data,
                                <double *>ccRd.data,
                                <double *>ccId.data,
                                nlags)
                return ccRd, ccId
    else:
        # If custom lags are provided, parse them into the format expected by spatcov
        nchans = I.shape[-1]
        assert len(customLags) == nlags, "Length of customLags must be equal to nlags."
        lagSizes = []
        ccIndexes = []
        for lags in customLags:
            lagSizes.append(len(lags))
            for cc in lags:
                assert len(cc) == 2, "Must provide a pair of channels to compute the covariance."
                ccIndexes.append(cc[0] + nchans*cc[1])
        lagsz = np.ascontiguousarray(np.array(lagSizes,dtype=np.uintp))
        ccidx = np.ascontiguousarray(np.array(ccIndexes,dtype=np.uintp))

        if I.dtype == np.dtype('float32'):
            # Initialize cdef arrays
            If = np.ascontiguousarray(I)
            Qf = np.ascontiguousarray(Q)
            ccRf = np.ascontiguousarray(np.zeros((I.shape[:-1] + (nlags,)),dtype=np.float32))
            if computeComplexCov == False:
                spatcov[float](mode,
                               ndims,
                               <size_t *>ddims.data,
                               <float  *>If.data,
                               <float  *>Qf.data,
                               <size_t *>kdims.data,
                               <float  *>ccRf.data,
                               NULL,
                               nlags,
                               <size_t *>lagsz.data,
                               <size_t *>ccidx.data)
                return ccRf
            else:
                ccIf = np.ascontiguousarray(np.zeros((I.shape[:-1] + (nlags,)),dtype=np.float32))
                spatcov[float](mode,
                               ndims,
                               <size_t *>ddims.data,
                               <float  *>If.data,
                               <float  *>Qf.data,
                               <size_t *>kdims.data,
                               <float  *>ccRf.data,
                               <float  *>ccIf.data,
                               nlags,
                               <size_t *>lagsz.data,
                               <size_t *>ccidx.data)
                return ccRf, ccIf
        elif I.dtype == np.dtype('float64'):
            # Initialize cdef arrays
            Id = np.ascontiguousarray(I)
            Qd = np.ascontiguousarray(Q)
            ccRd = np.ascontiguousarray(np.zeros((I.shape[:-1] + (nlags,)),dtype=np.float64))
            if computeComplexCov == False:
                spatcov[double](mode,
                                ndims,
                                <size_t *>ddims.data,
                                <double *>Id.data,
                                <double *>Qd.data,
                                <size_t *>kdims.data,
                                <double *>ccRd.data,
                                NULL,
                                nlags,
                                <size_t *>lagsz.data,
                                <size_t *>ccidx.data)
                return ccRd
            else:
                ccId = np.ascontiguousarray(np.zeros((I.shape[:-1] + (nlags,)),dtype=np.float64))
                spatcov[double](mode,
                                ndims,
                                <size_t *>ddims.data,
                                <double *>Id.data,
                                <double *>Qd.data,
                                <size_t *>kdims.data,
                                <double *>ccRd.data,
                                <double *>ccId.data,
                                nlags,
                                <size_t *>lagsz.data,
                                <size_t *>ccidx.data)
                return ccRd, ccId




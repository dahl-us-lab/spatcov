#ifndef SPATCOV_MEX_H
#define SPATCOV_MEX_H
/** Correlation coefficient estimation methods
 *
 */

#include "spatcov.h"
#include "mex.h"

#ifndef CCTYPE
#define CCTYPE 1
#endif

#endif // SPATCOV_MEX_H

%% Choose processing parameters
ksz = [1 1];
maxlags = [4, 8, 16, 32];
ccmode = 0;
dataFile = 'cyst_-20dB_lowRes.mat';
% dataFile = 'cyst_-20dB_hiRes.mat';

%% Estimate spatial covariance from Field II simulated data
cd ..
load(dataFile)

% If spatcovMT_mex cannot be found, just execute spatcov_mex
if exist('spatcovMT_mex', 'file') ~= 3
	fprintf('Multi-threaded mex function not detected. Using single-threaded version...\n')
	spatcovMT_mex = @(f,k,l,c) spatcov_mex(f,k,l,c);
end

% Show results in a figure
figure(2),clf;
set(gcf,'Position',[100 100 1200 300])

% Loop over number of lags
for i = 1:length(maxlags)
	maxlag = maxlags(i);
	% Compute spatial covariance
	cc = spatcovMT_mex(focsig, ksz, maxlag, ccmode);
	% Make SLSC image
	cimg = mean(cc,3);
	% Plot
	subplot(1,4,i)
	imagesc(img_x, img_z, cimg/max(cimg(:)), [0 1]); colormap gray; axis image
	title(sprintf('maxlag: %d', maxlag))
end

cd examples

%% Show an example of using make_lagDesc
cd ..
load(dataFile)

% If spatcovMT_mex cannot be found, just execute spatcov_mex
if exist('spatcovMT_mex', 'file') ~= 3
	fprintf('Multi-threaded mex function not detected. Using single-threaded version...\n')
	spatcovMT_mex = @(f,k,l,c) spatcov_mex(f,k,l,c);
end

% Show results in a figure
figure(3),clf;
set(gcf,'Position',[100 100 300 300])

lagDesc = make_lagDesc(128, 1, linspace(0.99, 0.75, 10));
% Compute spatial covariance
cc = spatcovMT_mex(focsig, ksz, lagDesc, ccmode);
% Make SLSC image
cimg = mean(cc,3);
% Plot
imagesc(img_x, img_z, cimg/max(cimg(:)), [0 1]); colormap gray; axis image

cd examples



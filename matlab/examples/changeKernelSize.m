%% Choose processing parameters
kszs = {[1 1], [1 5], [5 1], [5 5]};
maxlag = 16;
ccmode = 0;
dataFile = 'cyst_-20dB_lowRes.mat';
% dataFile = 'cyst_-20dB_hiRes.mat';

%% Estimate spatial covariance from Field II simulated data
cd ..
load(dataFile)

% If spatcovMT_mex cannot be found, just execute spatcov_mex
if exist('spatcovMT_mex', 'file') ~= 3
	fprintf('Multi-threaded mex function not detected. Using single-threaded version...\n')
	spatcovMT_mex = @(f,k,l,c) spatcov_mex(f,k,l,c);
end

% Show results in a figure
figure(1),clf;
set(gcf,'Position',[100 100 600 600])

% Loop over kernel sizes
for k = 1:length(kszs)
	ksz = kszs{k};
	% Compute spatial covariance
	cc = spatcovMT_mex(focsig, ksz, maxlag, ccmode);
	% Make SLSC image
	cimg = mean(cc,3);
	% Plot
	subplot(2,2,k)
	imagesc(img_x, img_z, cimg/max(cimg(:)), [0 1]); colormap gray; axis image
	title(sprintf('ksz: [%d %d]', ksz(1), ksz(2)))
end

cd examples

%% Choose processing parameters
ksz = [1 1];
maxlag = 16;
dataFile = 'cyst_-20dB_lowRes.mat';
% dataFile = 'cyst_-20dB_hiRes.mat';

%% Estimate spatial covariance from Field II simulated data
cd ..
load(dataFile)

% If spatcovMT_mex cannot be found, just execute spatcov_mex
if exist('spatcovMT_mex', 'file') ~= 3
	fprintf('Multi-threaded mex function not detected. Using single-threaded version...\n')
	spatcovMT_mex = @(f,k,l,c) spatcov_mex(f,k,l,c);
end

% Show results in a figure
figure(3),clf;
set(gcf,'Position',[100 100 600 300])

% Ensemble mode (ccmode == 0)
% Compute spatial covariance
cc = spatcovMT_mex(focsig, ksz, maxlag, 0);
% Make SLSC image
cimg = mean(cc,3);
% Plot
subplot(1,2,1)
imagesc(img_x, img_z, cimg/max(cimg(:)), [0 1]); colormap gray; axis image
title('Ensemble CC')

% Average mode (ccmode == 1)
% Compute spatial covariance
cc = spatcovMT_mex(focsig, ksz, maxlag, 1);
% Make SLSC image
cimg = mean(cc,3);
% Plot
subplot(1,2,2)
imagesc(img_x, img_z, cimg/max(cimg(:)), [0 1]); colormap gray; axis image
title('Average CC')

cd examples

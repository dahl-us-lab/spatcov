% spatcov_mex   Short-lag spatial coherence of IQ data.
%    [ccR, ccI] = spatcov_mex(iqdata, kernelsz, maxlag, ccmode) returns the
%       spatial correlation coefficients of the channel data.
%
%     This code is
%
%    Inputs:
%       iqdata    - Focused IQ data (Both RF and baseband are okay; just needs to
%                    be complex data). The data may be provided with an arbitrary
%                    number of dimensions, as long as the channels are the last
%                    dimension. Data should have dimension <npixels> x nchans.
%                    For example:
%                        2D data: depth x azimuth x nchans
%                        3D data: depth x azimuth x elevation x nchans
%                        4D data: depth x azimuth x elevation x frames x nchans
%       kernelsz -    The kernel over which correlation coefficients are to be
%                    computed. The kernel size is specified as the number of
%                    samples to use in each of the image dimensions. For example,
%                    a kernel size of [5 7 3] would apply a kernel of 5 samples
%                    in the first dimension, 7 samples in the second, and 3
%                    samples in the third. The default value for each dimension
%                    is 1.
%       maxlag   -    Description of the lags to be computed. If the value is a
%                    scalar, it is assumed that the channel data are from a 1D
%                    array, and that lagDesc is the maximum lag. The default is to
%                    assume a 1D array and to compute all lags. For advanced
%                    usage, see below.
%       ccmode   -    Selects between the averaged correlation coefficient (as
%                    defined by Lediju et al. in 2011) vs. the ensemble
%                    correlation coefficient (Hyun and Dahl, 2016).
%                    0 = ensemble, 1 = average
%     Outputs:
%        ccR         -    The real component of the spatial correlation is outputted
%                    as <npixels> x nlags.
%        ccI         -    The imaginary component of the spatial correlation is only
%                    computed if requested.
%
%
%       ADVANCED USAGE:
%          A custom lag scheme can be specified by replacing maxlag with a cell
%          array lagDesc. This can be useful when using a matrix array, or when
%          using a subset of the available aperture to compute correlations.
%
%          Each cell should contain a 2-column matrix listing all channel pairs
%           that should correspond to the same lag. For example, the lagDesc for
%           a 1D array would be computed as:
%
%                lagDesc = cell(maxlag, 1);
%                for m = 1:maxlag
%                    lagDesc{m} = cat(2, (1:nchans-m)', (m+1:nchans)');
%                end
%
%           For this lagDesc, the following two commands yield identical results:
%                ccR = spatcov_mex(iqdata, kernelsz, maxlag, ccmode);
%                ccR = spatcov_mex(iqdata, kernelsz, lagDesc, ccmode);
%
%			For 1.5D or 2D arrays, the make_lagDesc.m function provides a
%			 convenient way to form the lagDesc cell array.
%
%    NOTE: This code may return NaNs and Infs if the data is too small or large
%          (e.g., Field II data that is on the order of ~1e-20). If this occurs,
%          try normalizing the data:
%
%             iqdata = iqdata / max(abs(iqdata(:)));
%

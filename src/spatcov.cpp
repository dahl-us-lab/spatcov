#include "spatcov.h"

#include <cstdio>
#include <cstring>
#define MAX_NPDIMS 8

//----------------------------------------------------------------------
// INTERNAL FUNCTION DECLARATIONS
//----------------------------------------------------------------------
// Function to load a specified kernel from (I,Q) into (Ik,Qk)
template <typename T>
static void loadKernel(size_t npdims, const size_t *dataDims, T *I, T *Q,
                       size_t nchans, ptrdiff_t *dataIdx, ptrdiff_t *kernelTail,
                       T *Ik, T *Qk, ptrdiff_t *kernDims);
// Function to grab a truncated form of the tensors Idata and Qdata according
// to the dimension sizes in kernDims and places them in Ik and Qk,
// respectively. E.g., for kernDims = {10, 5, 18, 14}, we have (using MATLAB
// notation):
//		Ik = I(1:10, 1:5, 1:18, 1:14);
//		Qk = Q(1:10, 1:5, 1:18, 1:14);
// To accomplish this for an n-dimensional array, a recursive function is used.
// This function additionally transposes the kernel such that the innermost
// dimension is the channel dimension, for more efficient memory access
// patterns.
template <typename T>
static void grabTensorKernelT(ptrdiff_t ndims, const size_t *dims, T *Idata,
                              T *Qdata, ptrdiff_t *kernDims, T *Ik, T *Qk,
                              size_t nchans);
// Function that outputs pixel position in the n-dimensional array
static void getArrayIndexes(size_t overallIdx, size_t ndims, const size_t *dims,
                            ptrdiff_t *idx);
// Function that outputs vectorized index of position given by idx
static ptrdiff_t getOverallIndex(size_t ndims, const size_t *dims,
                                 ptrdiff_t *idx);
// Function that removes the kernel mean (for covariance computations)
template <typename T>
static void removeKernelMean(T *Ik, T *Qk, size_t nchans, size_t kernsz);
// CORE COHERENCE COMPUTATION FUNCTIONS
// Core function to compute the average correlation coefficient
// of a kernel of I and Q samples, assuming a 1D uniform array
template <typename T>
static void averageCorr1DArray(T *Ik, T *Qk, T *varX, size_t kernsz,
                               size_t nchans, T *ccR, T *ccI, size_t npixels,
                               size_t nlags);
// Core function to compute the average correlation coefficient
// of a kernel of I and Q samples with user-defined lags.
template <typename T>
static void averageCorrCustom(T *Ik, T *Qk, T *varX, size_t kernsz,
                              size_t nchans, T *ccR, T *ccI, size_t npixels,
                              size_t nlags, size_t *lagSizes,
                              size_t *ccIndexes);
// Core function to compute the ensemble correlation coefficient
// of a kernel of I and Q samples, assuming a 1D uniform array
template <typename T>
static void ensembleCorr1DArray(T *Ik, T *Qk, T *varX, size_t kernsz,
                                size_t nchans, T *ccR, T *ccI, size_t npixels,
                                size_t nlags);
// Core function to compute the ensemble correlation coefficient
// of a kernel of I and Q samples using custom lags
template <typename T>
static void ensembleCorrCustom(T *Ik, T *Qk, T *varX, size_t kernsz,
                               size_t nchans, T *ccR, T *ccI, size_t npixels,
                               size_t nlags, size_t *lagSizes,
                               size_t *ccIndexes);
// Core function to compute the sum of the absolute differences
// of a kernel of I and Q samples, assuming a 1D uniform array
template <typename T>
static void sumAbsDiff1DArray(T *Ik, T *Qk, T *varX, size_t kernsz,
                              size_t nchans, T *ccRp, size_t npixels,
                              size_t nlags);
// Core function to compute the sum of the absolute differences
// of a kernel of I and Q samples with user-defined lags.
template <typename T>
static void sumAbsDiffCustom(T *Ik, T *Qk, T *varX, size_t kernsz,
                             size_t nchans, T *ccRp, size_t npixels,
                             size_t nlags, size_t *lagSizes, size_t *ccIndexes);

// // Wrapper for BLAS calls
// #include "blas.h"
// void Tsyrk(int nchans, int kernsz, float *A, float *C);
// void Tsyrk(int nchans, int kernsz, double *A, double *C);

//----------------------------------------------------------------------
// INTERNAL IMPLEMENTATIONS
//----------------------------------------------------------------------
template <typename T>
void computeCoherence(int ccmode, int cctype, size_t ndims,
                      const size_t *dataDims, T *I, T *Q, size_t *kernDims,
                      T *ccR, T *ccI, size_t nlags, size_t *lagSizes,
                      size_t *ccIndexes) {
  // Get size of overall data, organized as (pixels x channels), where
  // pixels is some N-dimensional array
  size_t npdims = ndims - 1;  // Number of pixel dimensions (sans channels)
  size_t npixels = 1;
  for (size_t i = 0; i < npdims; i++) {
    npixels *= dataDims[i];
  }
  size_t nchans = dataDims[ndims - 1];

  // Compute the size of one tail of the kernel
  ptrdiff_t halfk[MAX_NPDIMS];
  for (size_t i = 0; i < npdims; i++) {
    if (kernDims[i] % 2 == 0) {
      printf(
          "Kernel size in dimension %d (%d) is even. Incrementing to %d...\n",
          int(i), int(kernDims[i]), int(kernDims[i] + 1));
      kernDims[i]++;
    }
    halfk[i] = (kernDims[i] - 1) / 2;
  }

  // Get size of the entire kernel
  size_t ksz = 1;
  for (size_t i = 0; i < npdims; i++) {
    ksz *= kernDims[i];
  }

  // Get number of OMP threads
  int nthreads = 1;
#ifdef ENABLE_MULTITHREADING
#pragma omp parallel
  { nthreads = omp_get_num_threads(); }
  printf("Running on %d threads...\n", nthreads);
#endif

  // Allocate memory for kernel
  T *Ik = (T *)malloc(sizeof(T) * ksz * nchans * nthreads);
  T *Qk = (T *)malloc(sizeof(T) * ksz * nchans * nthreads);
  T *varX = (T *)malloc(sizeof(T) * nchans * nthreads);

  // Loop through specified image region
#ifdef ENABLE_MULTITHREADING  // silence compiler warning
#pragma omp parallel for
#endif
  for (int p = 0; p < npixels; p++) {
    ptrdiff_t pixIdx[MAX_NPDIMS];
    getArrayIndexes(p, npdims, dataDims, pixIdx);

#ifdef ENABLE_MULTITHREADING
    int t = omp_get_thread_num();
#else
    int t = 0;
#endif
    // Place pointer for current thread at the appropriate memory location
    T *Ikt = &Ik[ksz * nchans * t];
    T *Qkt = &Qk[ksz * nchans * t];
    T *varXt = &varX[nchans * t];
    // Set kernel to zero
    memset(Ikt, 0, sizeof(T) * ksz * nchans);
    memset(Qkt, 0, sizeof(T) * ksz * nchans);
    // Load current kernel
    ptrdiff_t newKernDims[MAX_NPDIMS];
    loadKernel(npdims, dataDims, I, Q, nchans, pixIdx, halfk, Ikt, Qkt,
               newKernDims);
    // Get the dimension of the actually obtained kernel
    size_t newksz = 1;
    for (size_t i = 0; i < npdims; i++) {
      newksz *= newKernDims[i];
    }
    // Get pointers to the output position
    T *ccRt = &ccR[p];
    T *ccIt = ccI == NULL ? NULL : &ccI[p];
    // Compute the correlation coefficients
    // If the cctype is covariance, zero-mean the kernel
    if (cctype == 0) {
      // EXCEPT when the kernel size of 1, because this leads to
      // 0/0 = NaN for every computation.
      if (newksz > 1) removeKernelMean(Ikt, Qkt, nchans, newksz);
    }

    // Compute the desired coherence metric!
    if (lagSizes == NULL || ccIndexes == NULL) {  // Assume a 1D array
      if (cctype < 2) {
        if (ccmode == 0)
          ensembleCorr1DArray(Ikt, Qkt, varXt, newksz, nchans, ccRt, ccIt,
                              npixels, nlags);
        else
          averageCorr1DArray(Ikt, Qkt, varXt, newksz, nchans, ccRt, ccIt,
                             npixels, nlags);
      } else if (cctype == 2)
        sumAbsDiff1DArray(Ikt, Qkt, varXt, newksz, nchans, ccRt, npixels,
                          nlags);
    } else {  // Custom lags
      if (cctype < 2) {
        if (ccmode == 0)
          ensembleCorrCustom(Ikt, Qkt, varXt, newksz, nchans, ccRt, ccIt,
                             npixels, nlags, lagSizes, ccIndexes);
        else
          averageCorrCustom(Ikt, Qkt, varXt, newksz, nchans, ccRt, ccIt,
                            npixels, nlags, lagSizes, ccIndexes);
      } else if (cctype == 2)
        sumAbsDiffCustom(Ikt, Qkt, varXt, newksz, nchans, ccRt, npixels, nlags,
                         lagSizes, ccIndexes);
    }
  }

  // Free allocated memory
  free(Ik);
  free(Qk);
  free(varX);
}
// Explicit template instantiation
template void computeCoherence(int, int, size_t, const size_t *, double *,
                               double *, size_t *, double *, double *, size_t,
                               size_t *, size_t *);
template void computeCoherence(int, int, size_t, const size_t *, float *,
                               float *, size_t *, float *, float *, size_t,
                               size_t *, size_t *);

template <typename T>
double checkDataRMS(T *I, T *Q, size_t dataSize) {
  double sum = 0.0;
  for (int n = 0; n < dataSize; n++) {
    sum += I[n] * I[n] + Q[n] * Q[n];
  }
  return sqrt(sum / dataSize);
}
// Explicit instantiation of template
template double checkDataRMS(double *, double *, size_t);
template double checkDataRMS(float *, float *, size_t);

template <typename T>
void normalizeData(T *I, T *Q, size_t dataSize, double rmsValue) {
  for (int n = 0; n < dataSize; n++) {
    I[n] = T(I[n] / rmsValue);
    Q[n] = T(Q[n] / rmsValue);
  }
}
// Explicit instantiation of template
template void normalizeData(double *, double *, size_t, double);
template void normalizeData(float *, float *, size_t, double);

// Load n-dimensional tensor kernel
template <typename T>
static void loadKernel(size_t npdims, const size_t *dataDims, T *I, T *Q,
                       size_t nchans, ptrdiff_t *dataIdx, ptrdiff_t *kernelTail,
                       T *Ik, T *Qk, ptrdiff_t *kernDims) {
  for (size_t i = 0; i < npdims; i++) {
    kernDims[i] = 2 * kernelTail[i] + 1;
  }
  // For the current data index, find the valid edges of the kernel.
  ptrdiff_t negEdge[NPDIMS_MAX];
  ptrdiff_t posEdge[NPDIMS_MAX];
  for (size_t d = 0; d < npdims; d++) {
    // Desired edges
    ptrdiff_t neg = dataIdx[d] - kernelTail[d];
    ptrdiff_t pos = dataIdx[d] + kernelTail[d];
    // Clip at edge of data and store
    negEdge[d] = neg >= 0 ? neg : 0;  // Negative edge can't go below zero.
    posEdge[d] = pos < ptrdiff_t(dataDims[d])
                     ? pos
                     : ptrdiff_t(dataDims[d]) -
                           1;  // Positive edge can't exceed data size.
    // Store true kernel size as well
    kernDims[d] = posEdge[d] - negEdge[d] + 1;
  }

  // Compute overall data size
  size_t npixels = 1;
  for (size_t i = 0; i < npdims; i++) {
    npixels *= dataDims[i];
  }
  // Obtain the offset corresponding to the kernel start
  ptrdiff_t offset = getOverallIndex(npdims, dataDims, negEdge);

  // Load the kernel such that channels are the fastest changing dimension
  for (size_t chan = 0; chan < nchans; chan++) {
    grabTensorKernelT(npdims, dataDims, &I[offset + chan * npixels],
                      &Q[offset + chan * npixels], kernDims, &Ik[chan],
                      &Qk[chan], nchans);
  }
}

// This function grabs a truncated form of the tensors Idata and Qdata according
// to the dimension sizes in kernDims and places them in Ik and Qk,
// respectively. E.g., for kernDims = {10, 5, 18, 14}, we have (using MATLAB
// notation):
//		Ik = I(1:10, 1:5, 1:18, 1:14);
//		Qk = Q(1:10, 1:5, 1:18, 1:14);
// To accomplish this for an n-dimensional array, a recursive function is used.
// This function additionally transposes the kernel such that the innermost
// dimension is the channel dimension, for more efficient memory access
// patterns.
template <typename T>
static void grabTensorKernelT(ptrdiff_t ndims, const size_t *dims, T *Idata,
                              T *Qdata, ptrdiff_t *kernDims, T *Ik, T *Qk,
                              size_t nchans) {
  // Tensor rank of the sub-tensor
  ptrdiff_t nsubd = ndims - 1;
  for (ptrdiff_t x = 0; x < kernDims[nsubd]; x++) {
    if (nsubd == 0) {
      // If the array is 1-dimensional, just copy the data
      Ik[x * nchans] = Idata[x];
      Qk[x * nchans] = Qdata[x];
    } else if (nsubd > 0) {
      // Otherwise, for each x in current dimension, grab the subtensor
      // of dimension ndims-1.
      // Compute the data and kernel subtensor sizes for proper indexing.
      size_t dataSubSize = 1;
      size_t kernSubSize = 1;
      for (auto i = 0; i < nsubd; i++) {
        dataSubSize *= dims[i];
        kernSubSize *= kernDims[i];
      }
      // Call grabSubTensorT recursively to get sub-tensors.
      grabTensorKernelT(nsubd, dims, &Idata[x * dataSubSize],
                        &Qdata[x * dataSubSize], kernDims,
                        &Ik[x * kernSubSize * nchans],
                        &Qk[x * kernSubSize * nchans], nchans);
    }
  }
}

static void getArrayIndexes(size_t overallIdx, size_t ndims, const size_t *dims,
                            ptrdiff_t *idx) {
  if (ndims == 1) {
    idx[ndims - 1] = overallIdx;
  } else {
    // Get frame size of sub-tensor
    size_t subSize = 1;
    for (size_t i = 0; i < ndims - 1; i++) {
      subSize *= dims[i];
    }
    idx[ndims - 1] = overallIdx / subSize;
    getArrayIndexes(overallIdx % subSize, ndims - 1, dims, idx);
  }
}

static ptrdiff_t getOverallIndex(size_t ndims, const size_t *dims,
                                 ptrdiff_t *idx) {
  ptrdiff_t index = idx[ndims - 1];
  for (ptrdiff_t i = ndims - 2; i >= 0; i--) index = index * dims[i] + idx[i];
  return index;
}

#define MAX_NCHANS 1024
template <typename T>
static void removeKernelMean(T *Ik, T *Qk, size_t nchans, size_t kernsz) {
  double sumI[MAX_NCHANS], sumQ[MAX_NCHANS];
  // Initialize to zero
  for (size_t chan = 0; chan < nchans; chan++) {
    sumI[chan] = 0.0;
    sumQ[chan] = 0.0;
  }
  // Compute the sum of each kernel for each channel
  for (size_t i = 0; i < kernsz; i++) {
    for (size_t chan = 0; chan < nchans; chan++) {
      sumI[chan] += (double)Ik[chan + nchans * i];
      sumQ[chan] += (double)Qk[chan + nchans * i];
    }
  }
  // Convert sum to average
  for (size_t chan = 0; chan < nchans; chan++) {
    sumI[chan] /= (double)kernsz;
    sumQ[chan] /= (double)kernsz;
  }
  // Subtract the average from each sample
  for (size_t i = 0; i < kernsz; i++) {
    for (size_t chan = 0; chan < nchans; chan++) {
      Ik[chan + nchans * i] -= (T)sumI[chan];
      Qk[chan + nchans * i] -= (T)sumQ[chan];
    }
  }
}

template <typename T>
static void averageCorr1DArray(T *Ik, T *Qk, T *varX, size_t kernsz,
                               size_t nchans, T *ccR, T *ccI, size_t npixels,
                               size_t nlags) {
  // Compute the variance of each channel over the kernel and pre-normalize
  for (size_t x = 0; x < nchans; x++) {
    varX[x] = (T)0.0;
    for (size_t k = 0; k < kernsz; k++)
      varX[x] += Ik[x + nchans * k] * Ik[x + nchans * k] +
                 Qk[x + nchans * k] * Qk[x + nchans * k];
    for (size_t k = 0; k < kernsz; k++) {
      Ik[x + nchans * k] = T(Ik[x + nchans * k] / sqrt(varX[x]));
      Qk[x + nchans * k] = T(Qk[x + nchans * k] / sqrt(varX[x]));
    }
  }
  // Compute correlation coefficients
  for (size_t lag = 1; lag <= nlags; lag++) {
    T sumR = (T)0.0;
    T sumI = (T)0.0;
    for (size_t x = 0; x < nchans - lag; x++) {
      size_t y = x + lag;
      T sumXY = (T)0.0;
      for (size_t k = 0; k < kernsz; k++) {
        sumXY += Ik[x + nchans * k] * Ik[y + nchans * k] +
                 Qk[x + nchans * k] * Qk[y + nchans * k];
      }
      sumR += isnan(sumXY) ? (T)0.0 : sumXY;
      if (ccI != NULL) {
        T sumYX = (T)0.0;
        for (size_t k = 0; k < kernsz; k++) {
          sumYX += Qk[x + nchans * k] * Ik[y + nchans * k] -
                   Ik[x + nchans * k] * Qk[y + nchans * k];
        }
        sumI += isnan(sumYX) ? (T)0.0 : sumYX;
      }
    }
    ccR[npixels * (lag - 1)] = sumR / (nchans - lag);
    if (ccI != NULL) {
      ccI[npixels * (lag - 1)] = sumI / (nchans - lag);
    }
  }
}

template <typename T>
static void averageCorrCustom(T *Ik, T *Qk, T *varX, size_t kernsz,
                              size_t nchans, T *ccR, T *ccI, size_t npixels,
                              size_t nlags, size_t *lagSizes,
                              size_t *ccIndexes) {
  // Compute the variance of each channel over the kernel and pre-normalize
  for (size_t x = 0; x < nchans; x++) {
    varX[x] = (T)0.0;
    for (size_t k = 0; k < kernsz; k++)
      varX[x] += Ik[x + nchans * k] * Ik[x + nchans * k] +
                 Qk[x + nchans * k] * Qk[x + nchans * k];
    for (size_t k = 0; k < kernsz; k++) {
      Ik[x + nchans * k] = T(Ik[x + nchans * k] / sqrt(varX[x]));
      Qk[x + nchans * k] = T(Qk[x + nchans * k] / sqrt(varX[x]));
    }
  }
  // Compute correlation coefficients
  size_t *ccIdx = ccIndexes;  // Put a pointer at the beginning of ccIndexes
  for (size_t lag = 0; lag < nlags; lag++) {
    T sumR = (T)0.0;
    T sumI = (T)0.0;
    size_t lagSize = lagSizes[lag];
    for (size_t n = 0; n < lagSize; n++) {
      T sumXY = (T)0.0;
      size_t x = ccIdx[n] % nchans;
      size_t y = ccIdx[n] / nchans;
      for (size_t k = 0; k < kernsz; k++)
        sumXY += Ik[x + nchans * k] * Ik[y + nchans * k] +
                 Qk[x + nchans * k] * Qk[y + nchans * k];
      sumR += isnan(sumXY) ? (T)0.0 : sumXY;
      if (ccI != NULL) {
        T sumYX = (T)0.0;
        for (size_t k = 0; k < kernsz; k++) {
          sumYX += Qk[x + nchans * k] * Ik[y + nchans * k] -
                   Ik[x + nchans * k] * Qk[y + nchans * k];
        }
        sumI += isnan(sumYX) ? (T)0.0 : sumYX;
      }
    }
    ccR[npixels * lag] = sumR / lagSize;
    if (ccI != NULL) {
      ccI[npixels * lag] = sumI / lagSize;
    }
    // Advance ccIdx pointer
    ccIdx += lagSize;
  }
}

template <typename T>
static void ensembleCorr1DArray(T *Ik, T *Qk, T *varX, size_t kernsz,
                                size_t nchans, T *ccR, T *ccI, size_t npixels,
                                size_t nlags) {
  // Compute the variance of each channel over the kernel
  for (size_t x = 0; x < nchans; x++) {
    varX[x] = (T)0.0;
    for (size_t k = 0; k < kernsz; k++)
      varX[x] += Ik[x + nchans * k] * Ik[x + nchans * k] +
                 Qk[x + nchans * k] * Qk[x + nchans * k];
  }
  // Compute correlation coefficients
  for (size_t lag = 1; lag <= nlags; lag++) {
    T sumXY, sumYX, sumXX, sumYY;
    sumXY = sumYX = sumXX = sumYY = (T)0.0;
    for (size_t x = 0; x < nchans - lag; x++) {
      sumXX += varX[x];
      sumYY += varX[x + lag];
    }
    for (size_t k = 0; k < kernsz; k++) {
      for (size_t x = 0; x < nchans - lag; x++) {
        size_t y = x + lag;
        sumXY += Ik[x + nchans * k] * Ik[y + nchans * k] +
                 Qk[x + nchans * k] * Qk[y + nchans * k];
      }
    }
    ccR[npixels * (lag - 1)] = T(sumXY / sqrt(sumXX * sumYY));
    // Compute imaginary component if requested
    if (ccI != NULL) {
      for (size_t k = 0; k < kernsz; k++) {
        for (size_t x = 0; x < nchans - lag; x++) {
          size_t y = x + lag;
          sumYX += Qk[x + nchans * k] * Ik[y + nchans * k] -
                   Ik[x + nchans * k] * Qk[y + nchans * k];
        }
      }
      ccI[npixels * (lag - 1)] = T(sumYX / sqrt(sumXX * sumYY));
    }
  }
}

template <typename T>
static void ensembleCorrCustom(T *Ik, T *Qk, T *varX, size_t kernsz,
                               size_t nchans, T *ccR, T *ccI, size_t npixels,
                               size_t nlags, size_t *lagSizes,
                               size_t *ccIndexes) {
  // Compute the variance of each channel over the kernel
  for (size_t x = 0; x < nchans; x++) {
    varX[x] = (T)0.0;
    for (size_t k = 0; k < kernsz; k++)
      varX[x] += Ik[x + nchans * k] * Ik[x + nchans * k] +
                 Qk[x + nchans * k] * Qk[x + nchans * k];
  }
  // Compute correlation coefficients
  size_t *ccIdx = ccIndexes;  // Put a pointer at the beginning of ccIndexes
  for (size_t lag = 0; lag < nlags; lag++) {
    T sumXY, sumYX, sumXX, sumYY;
    sumXY = sumYX = sumXX = sumYY = (T)0.0;
    size_t lagSize = lagSizes[lag];
    // Compute variances first
    for (size_t n = 0; n < lagSize; n++) {
      size_t x = ccIdx[n] % nchans;
      size_t y = ccIdx[n] / nchans;
      sumXX += varX[x];
      sumYY += varX[y];
    }
    // Compute covariances
    for (size_t k = 0; k < kernsz; k++) {
      for (size_t n = 0; n < lagSize; n++) {
        size_t x = ccIdx[n] % nchans;
        size_t y = ccIdx[n] / nchans;
        T tmp = Ik[x + nchans * k] * Ik[y + nchans * k] +
                Qk[x + nchans * k] * Qk[y + nchans * k];
        sumXY += isnan(tmp) ? (T)0.0 : tmp;
      }
    }
    // Compute correlation coefficient (covariance / sqrt(prod(variances)))
    ccR[npixels * lag] = T(sumXY / sqrt(sumXX * sumYY));

    // Compute imaginary component of correlation coefficient if requested
    if (ccI != NULL) {
      for (size_t k = 0; k < kernsz; k++) {
        for (size_t n = 0; n < lagSize; n++) {
          size_t x = ccIdx[n] % nchans;
          size_t y = ccIdx[n] / nchans;
          T tmp = Qk[x + nchans * k] * Ik[y + nchans * k] -
                  Ik[x + nchans * k] * Qk[y + nchans * k];
          sumYX += isnan(tmp) ? (T)0.0 : tmp;
        }
      }
      ccI[npixels * lag] = T(sumYX / sqrt(sumXX * sumYY));
    }
    // Advance ccIdx pointer
    ccIdx += lagSize;
  }
}

template <typename T>
static void sumAbsDiff1DArray(T *Ik, T *Qk, T *varX, size_t kernsz,
                              size_t nchans, T *ccR, size_t npixels,
                              size_t nlags) {
  // Compute the variance of each channel over the kernel
  for (size_t x = 0; x < nchans; x++) {
    varX[x] = (T)0.0;
    for (size_t k = 0; k < kernsz; k++)
      varX[x] += Ik[x + nchans * k] * Ik[x + nchans * k] +
                 Qk[x + nchans * k] * Qk[x + nchans * k];
    for (size_t k = 0; k < kernsz; k++) {
      Ik[x + nchans * k] = T(Ik[x + nchans * k] / sqrt(varX[x]));
      Qk[x + nchans * k] = T(Qk[x + nchans * k] / sqrt(varX[x]));
    }
  }
  // Compute the sum absolute difference over the kernels
  for (size_t lag = 1; lag <= nlags; lag++) {
    T sumR = (T)0.0;
    for (size_t x = 0; x < nchans - lag; x++) {
      size_t y = x + lag;
      for (size_t k = 0; k < kernsz; k++) {
        T diffI = Ik[x + nchans * k] - Ik[y + nchans * k];
        T diffQ = Qk[x + nchans * k] - Qk[y + nchans * k];
        T absDiff = T(sqrt(diffI * diffI + diffQ * diffQ));
        sumR += isnan(absDiff) ? (T)0.0 : absDiff;
      }
    }
    ccR[npixels * (lag - 1)] = 1 - (sumR / (nchans - lag) / kernsz);
  }
}

template <typename T>
static void sumAbsDiffCustom(T *Ik, T *Qk, T *varX, size_t kernsz,
                             size_t nchans, T *ccR, size_t npixels,
                             size_t nlags, size_t *lagSizes,
                             size_t *ccIndexes) {
  // Compute the variance of each channel over the kernel
  for (size_t x = 0; x < nchans; x++) {
    varX[x] = (T)0.0;
    for (size_t k = 0; k < kernsz; k++)
      varX[x] += Ik[x + nchans * k] * Ik[x + nchans * k] +
                 Qk[x + nchans * k] * Qk[x + nchans * k];
    for (size_t k = 0; k < kernsz; k++) {
      Ik[x + nchans * k] = T(Ik[x + nchans * k] / sqrt(varX[x]));
      Qk[x + nchans * k] = T(Qk[x + nchans * k] / sqrt(varX[x]));
    }
  }
  // Compute correlation coefficients
  size_t *ccIdx = ccIndexes;  // Put a pointer at the beginning of ccIndexes
  for (size_t lag = 0; lag < nlags; lag++) {
    T sumR = (T)0.0;
    size_t lagSize = lagSizes[lag];
    for (size_t n = 0; n < lagSize; n++) {
      size_t x = ccIdx[n] % nchans;
      size_t y = ccIdx[n] / nchans;
      for (size_t k = 0; k < kernsz; k++) {
        T diffI = Ik[x + nchans * k] - Ik[y + nchans * k];
        T diffQ = Qk[x + nchans * k] - Qk[y + nchans * k];
        T absDiff = T(sqrt(diffI * diffI + diffQ * diffQ));
        sumR += isnan(absDiff) ? (T)0.0 : absDiff;
      }
    }
    ccR[npixels * lag] = 1 - (sumR / lagSize / kernsz);
    // Advance ccIdx pointer
    ccIdx += lagSize;
  }
}

//----------------------------------------------------------------------
// FUNCTION IMPLEMENTATIONS
//----------------------------------------------------------------------
template <typename T>
void spatcov(int ccmode, size_t ndims, const size_t *dataDims, T *I, T *Q,
             size_t *kernDims, T *ccR, T *ccI, size_t nlags, size_t *lagSizes,
             size_t *ccIndexes) {
  int cctype = 0;
  computeCoherence(ccmode, cctype, ndims, dataDims, I, Q, kernDims, ccR, ccI,
                   nlags, lagSizes, ccIndexes);
}
template <typename T>
void spatcor(int ccmode, size_t ndims, const size_t *dataDims, T *I, T *Q,
             size_t *kernDims, T *ccR, T *ccI, size_t nlags, size_t *lagSizes,
             size_t *ccIndexes) {
  int cctype = 1;
  computeCoherence(ccmode, cctype, ndims, dataDims, I, Q, kernDims, ccR, ccI,
                   nlags, lagSizes, ccIndexes);
}
template <typename T>
void spatsad(int ccmode, size_t ndims, const size_t *dataDims, T *I, T *Q,
             size_t *kernDims, T *ccR, T *ccI, size_t nlags, size_t *lagSizes,
             size_t *ccIndexes) {
  int cctype = 2;
  computeCoherence(ccmode, cctype, ndims, dataDims, I, Q, kernDims, ccR, ccI,
                   nlags, lagSizes, ccIndexes);
}

// Vestiges of an old implementation. Need to decide whether to keep
// the BLAS implementation or not.
// void Tsyrk(int nchans, int kernsz, float *A, float *C) {
// 	char uplo = 'U';
// 	char tran = 'N';
// 	ptrdiff_t N = nchans;
// 	ptrdiff_t K = kernsz;
// 	float one = 1.0f;
// 	float zero = 0.0f;
// 	ssyrk_(&uplo, &tran, &N, &K, &one, A, &N, &zero, C, &N);
// }
// void Tsyrk(int nchans, int kernsz, double *A, double *C) {
// 	char uplo = 'U';
// 	char tran = 'N';
// 	ptrdiff_t N = nchans;
// 	ptrdiff_t K = kernsz;
// 	double one = 1.0;
// 	double zero = 0.0;
// 	dsyrk_(&uplo, &tran, &N, &K, &one, A, &N, &zero, C, &N);
// }

#ifndef SPATCOV_H
#define SPATCOV_H
/** Correlation coefficient estimation methods
 *  2016-08-29  Dongwoon Hyun (created)
 */

#include <cstdlib>
#include <cstddef>
#include <math.h>
#ifdef ENABLE_MULTITHREADING
#include <omp.h>
#endif

#define NPDIMS_MAX 8  // A maximum of n-dimensional array allowed

// MAIN FUNCTION
// Driver function to compute correlation coefficients
// Inputs:
//		ccmode		Wide-sense stationarity aggregation mode
// 						0: ensemble mode
// 						1: averaging mode
//		cctype		Spatial coherence computation
// 						0: covariance (i.e. subtracting mean of kernel)
//						1: correlation (no subtraction)
//						2: sum of absolute differences
//		ndims		Number of overall data dimensions, including channel dimension
//		dataDims	Dimensions of the data (array of length ndims)
//		I			In-phase data, with dimension dataDims, organized as
//						[<pixels>, channels]. <pixels> can be an n-dimensional array
// 						organized as [x], [x,y], [x,y,z], [x,y,z,w], etc.
//		Q			Quadrature data, with dimension dataDims.
//		kernDims	Kernel size in each pixel dimension. kernDims must be an array
//						of length ndims-1. Kernel sizes should be odd numbers.
//		ccR			Real component of the complex correlation coefficient, organized as
//						[<pixels>, nlags].
//		ccI			Imaginary component of the complex correlation coefficient, organized as
//						[<pixels>, nlags]. This value is only computed if ccI != NULL.
//		nlags		Number of lags to compute.
// Optional Inputs:
//		lagSizes	The number of correlation coefficients in ccIndexes that correspond to
//						each lag.
//		ccIndexes	The covariance matrix indexes for each requested element pair. For each
//						lag m, a total lagSizes[m] correlations are aggregated into a single
//						correlation coefficient value.
//
// This function accepts inputs of type double and float.
template <typename T>
void computeCoherence(int ccmode,
					  int cctype,
					  size_t ndims,
					  const size_t *dataDims,
					  T *I,
					  T *Q,
					  size_t *kernDims,
					  T *ccR,
					  T *ccI,
					  size_t nlags,
					  size_t *lagSizes,
					  size_t *ccIndexes);

// For convenience, explicit functions are available for each cctype.
template <typename T>
void spatcov(int ccmode,
			 size_t ndims,
			 const size_t *dataDims,
			 T *I,
			 T *Q,
			 size_t *kernDims,
			 T *ccR,
			 T *ccI,
			 size_t nlags,
			 size_t *lagSizes=NULL,
			 size_t *ccIndexes=NULL);
template <typename T>
void spatcor(int ccmode,
			 size_t ndims,
			 const size_t *dataDims,
			 T *I,
			 T *Q,
			 size_t *kernDims,
			 T *ccR,
			 T *ccI,
			 size_t nlags,
			 size_t *lagSizes=NULL,
			 size_t *ccIndexes=NULL);
template <typename T>
void spatsad(int ccmode,
			 size_t ndims,
			 const size_t *dataDims,
			 T *I,
			 T *Q,
			 size_t *kernDims,
			 T *ccR,
			 T *ccI,
			 size_t nlags,
			 size_t *lagSizes=NULL,
			 size_t *ccIndexes=NULL);

// UTILITIES
// Function to check RMS value of data
template <typename T>
double checkDataRMS(T *I,
                    T *Q,
                    size_t dataSize);
// Function to normalize the data to avoid precision issues
template <typename T>
void normalizeData(T *I,
				   T *Q,
				   size_t dataSize,
				   double rmsValue);

#endif // SPATCOV_H

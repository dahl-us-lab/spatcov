# spatcov - Fast spatial coherence estimation

`spatcov` is a library of optimized C++ functions used to compute the spatial covariance of focused ultrasound data. A MATLAB/MEX interface is available, and can be used to accelerate computations from within the MATLAB environment. (A Python interface is in development.)

## Getting spatcov_mex

[2022-08-24] Precompiled MEX functions are now available on the [Releases](https://gitlab.com/dahl-us-lab/public/spatcov/-/releases) page! Single-threaded and multi-threaded versions are available for both Windows and Linux.

## Usage

Examples are provided in the `matlab/examples` folder. An example of usage would be:

```matlab
cc = spatcov_mex(focsig);
```

Here, `focsig` is a set of complex channel data focused in an *N*-dimensional pixel grid. (For conventional 2D scans, *N*=2.) The output, `cc`, contains the correlation coefficients amongst the channels, averaged as a function of channel lag, at all pixel points.

For example, if `focsig` is 100 x 50 x 64 in size (100 rows, 50 columns, 64 channels), `cc` will be 100 x 50 x 63, where `cc(:,:,i)` will give the average correlation coefficient between all channel pairs with a spacing of `i`. To make an SLSC image, simply combine the *short* lags:

```matlab
slsc = sum(cc(:,:,1:M), 3);
```

More detailed documentation can be found by executing `help spatcov_mex`. The multi-threaded version can be called by replacing `spatcov_mex` with `spatcovMT_mex`.

## Motivation

Spatial coherence estimation is a useful tool that is used in a variety of applications, most notably in **short-lag spatial coherence (SLSC)** imaging and in **backscatter tensor imaging (BTI)**. The spatial coherence of a wavefront describes the similarity between any two points along the wavefront, and can be used to infer properties about the source, such as its anisotropy or the lateral source magnitude. It has been used successfully in SLSC imaging to suppress clutter in a variety of applications including fetal, liver, and cardiac imaging.

Naive MATLAB implementations of spatial coherence estimation are generally slow. The `spatcov` library provides single- and multi-threaded implementations that are callable from MATLAB for speed improvements up to 2 orders in magnitude.

In particular, this code is an implementation of the efficient techniques described in Hyun et al., 2016 (see reference below), and was used to generate the images and results in that article.

## Manual installation instructions

Compilation requires CMake version >= 3.0.0.

1. Clone the repository into the desired directory:

```bash
git clone git@gitlab.com:dahl-us-lab/utilities/spatcov.git
```

2. Generate Makefiles using CMake:

```bash
cmake .
```

3. Build library:

```bash
make
```

4. (Optional) Download sample data sets for the MATLAB examples from <https://zenodo.org/record/254463>. The files should be placed in the `matlab/` subdirectory.

## Citing this work

This code is free to use, and is covered by the Apache v2 license. Please cite the following two references if this code is used to publish results:

* M. A. Lediju, G. E. Trahey, B. C. Byram and J. J. Dahl, "Short-lag spatial coherence of backscattered echoes: imaging characteristics," in *IEEE Transactions on Ultrasonics, Ferroelectrics, and Frequency Control*, vol. 58, no. 7, pp. 1377-1388, July 2011. doi: 10.1109/TUFFC.2011.1957
* D. Hyun, A. C. Crowley and J. J. Dahl, "Efficient Strategies for Estimating the Spatial Coherence of Backscatter," in *IEEE Transactions on Ultrasonics, Ferroelectrics, and Frequency Control*, vol.64, no.3, pp.500-513. doi: 10.1109/TUFFC.2016.2634004
